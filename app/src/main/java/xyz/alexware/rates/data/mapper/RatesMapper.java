package xyz.alexware.rates.data.mapper;

import java.math.BigDecimal;
import java.util.Map;

import javax.inject.Inject;

import xyz.alexware.rates.data.model.RatesResponse;
import xyz.alexware.rates.domain.model.CurrencyRate;
import xyz.alexware.rates.domain.model.CurrencyRateList;

public class RatesMapper {

    @Inject
    RatesMapper() {
    }

    public CurrencyRateList mapToDomain(RatesResponse ratesResponse) {
        CurrencyRateList currencyRates = new CurrencyRateList();
        Map<String, BigDecimal> rates = ratesResponse.getRates();
        for (Map.Entry<String, BigDecimal> rateEntry : rates.entrySet()) {
            String code = rateEntry.getKey();
            BigDecimal rate = rateEntry.getValue();
            CurrencyRate currencyRate = new CurrencyRate(code, rate);
            currencyRates.add(currencyRate);
        }
        return currencyRates;
    }
}
