package xyz.alexware.rates.data;

import java.math.BigDecimal;

import io.reactivex.Single;
import xyz.alexware.rates.data.mapper.RatesMapper;
import xyz.alexware.rates.data.service.CurrencyRatesService;
import xyz.alexware.rates.domain.model.CurrencyRate;
import xyz.alexware.rates.domain.model.CurrencyRateList;
import xyz.alexware.rates.domain.repository.CurrencyRatesRepository;

public class NetworkCurrencyRatesRepository implements CurrencyRatesRepository {

    private final CurrencyRatesService retrofitService;
    private final RatesMapper ratesMapper;

    public NetworkCurrencyRatesRepository(CurrencyRatesService currencyRatesService, RatesMapper ratesMapper) {
        this.retrofitService = currencyRatesService;
        this.ratesMapper = ratesMapper;
    }

    @Override
    public Single<CurrencyRateList> getLatestRates(String baseCurrency) {
        return retrofitService.getLatestRates(baseCurrency)
                .map(ratesMapper::mapToDomain)
                .map(currencyRates -> {
                    addBaseCurrencyRate(baseCurrency, currencyRates);
                    return currencyRates;
                });
    }

    private void addBaseCurrencyRate(String baseCurrency, CurrencyRateList currencyRates) {
        if (currencyRates.get(baseCurrency) == null) {
            currencyRates.add(0, new CurrencyRate(baseCurrency, BigDecimal.ONE));
        }
    }
}
