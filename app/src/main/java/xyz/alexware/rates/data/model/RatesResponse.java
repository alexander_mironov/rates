package xyz.alexware.rates.data.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

public class RatesResponse {
    private String base;
    private Date date;
    private Map<String, BigDecimal> rates;

    public String getBase() {
        return base;
    }

    public Date getDate() {
        return date;
    }

    public Map<String, BigDecimal> getRates() {
        return rates;
    }
}
