package xyz.alexware.rates.data.service;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;
import xyz.alexware.rates.data.model.RatesResponse;

public interface CurrencyRatesService {

    @GET("latest")
    Single<RatesResponse> getLatestRates(@Query("base") String baseCurrency);
}
