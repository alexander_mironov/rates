package xyz.alexware.rates;

import android.app.Application;

import xyz.alexware.rates.di.ApplicationComponent;
import xyz.alexware.rates.di.DaggerApplicationComponent;

public class CurrencyRateApplication extends Application {
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.create();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
