package xyz.alexware.rates.presentation.model;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

public class CurrencyRateView {
    private final CurrencyView currencyView;
    private final String calculatedValue;

    public CurrencyRateView(CurrencyView currencyView, String calculatedValue) {
        this.currencyView = currencyView;
        this.calculatedValue = calculatedValue;
    }

    public String getCalculatedValue() {
        return calculatedValue;
    }

    public String getCode() {
        return currencyView.getCode();
    }

    @StringRes
    public int getNameRes() {
        return currencyView.getNameRes();
    }

    @DrawableRes
    public int getIconRes() {
        return currencyView.getIconRes();
    }


}
