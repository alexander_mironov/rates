package xyz.alexware.rates.presentation.rates;

import android.support.annotation.NonNull;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import xyz.alexware.rates.domain.interactor.rates.GetRates;
import xyz.alexware.rates.domain.model.CurrencyRate;
import xyz.alexware.rates.domain.model.CurrencyRateList;
import xyz.alexware.rates.presentation.mapper.CurrencyPresentationFactory;
import xyz.alexware.rates.presentation.model.CurrencyRateView;
import xyz.alexware.rates.presentation.model.CurrencyView;
import xyz.alexware.rates.presentation.model.ViewState;

@Singleton
public class CurrencyRatePresenter {

    private final GetRates getRates;
    private final CurrencyPresentationFactory currencyPresentationFactory;
    private String baseCurrencyCode = "EUR";
    private BigDecimal baseValue = BigDecimal.ONE;
    private List<String> currenciesOrder = new ArrayList<>();
    private BehaviorSubject<ViewState> viewStateSubject = BehaviorSubject.create();

    @Inject
    CurrencyRatePresenter(GetRates getRates, CurrencyPresentationFactory currencyPresentationFactory) {
        this.getRates = getRates;
        this.currencyPresentationFactory = currencyPresentationFactory;
    }

    public Observable<ViewState> getViewState() {
        return viewStateSubject;
    }

    public void onCurrencySelected(String currencyRate) {
        changeBaseCurrency(currencyRate);
    }

    private void changeBaseCurrency(String baseCurrencyCode) {
        this.baseCurrencyCode = baseCurrencyCode;
        makeFirstInOrder(baseCurrencyCode);
        startUpdatingRates();
    }

    private void makeFirstInOrder(String baseCurrencyCode) {
        int prevPos = currenciesOrder.indexOf(baseCurrencyCode);
        currenciesOrder.remove(prevPos);
        currenciesOrder.add(0, baseCurrencyCode);
    }

    public void startUpdatingRates() {
        getRates.clear();
        getRates.execute(getRatesResult -> {
            switch (getRatesResult.getState()) {
                case NORMAL:
                    handleRates(getRatesResult.getData());
                    break;
                case ERROR:
                    handleError(getRatesResult.getError());
                    break;
            }
        }, baseCurrencyCode);
    }

    public void stopUpdatingRates() {
        getRates.clear();
    }

    private void handleRates(CurrencyRateList currencyRates) {
        if (firstLoad()) {
            setupInitialOrder(currencyRates);
        }
        List<CurrencyRateView> rateViews = mapToOrderedViews(currencyRates);
        viewStateSubject.onNext(ViewState.success(rateViews));
    }

    private void handleError(Throwable error) {
        viewStateSubject.onNext(ViewState.error(error));
    }

    private boolean firstLoad() {
        return currenciesOrder.size() == 0;
    }

    private void setupInitialOrder(List<CurrencyRate> currencyRates) {
        for (CurrencyRate currencyRate : currencyRates) {
            currenciesOrder.add(currencyRate.getCurrencyCode());
        }
    }

    @NonNull
    private List<CurrencyRateView> mapToOrderedViews(CurrencyRateList currencyRates) {
        List<CurrencyRateView> rateViews = new ArrayList<>();
        for (String currencyCode : currenciesOrder) {
            CurrencyRate currencyRate = currencyRates.get(currencyCode);
            CurrencyRateView currencyRateView = createCurrencyRateView(currencyCode, currencyRate);
            if (currencyRateView != null) {
                rateViews.add(currencyRateView);
            }
        }
        return rateViews;
    }

    private CurrencyRateView createCurrencyRateView(String currencyCode, CurrencyRate currencyRate) {
        if (currencyRate != null) {
            CurrencyView currencyView = currencyPresentationFactory.getCurrencyView(currencyCode);
            String calculatedValueString = calculateFinalValue(currencyRate);
            return new CurrencyRateView(currencyView, calculatedValueString);
        }
        return null;
    }

    private String calculateFinalValue(CurrencyRate currencyRate) {
        BigDecimal calculatedValue = baseValue.multiply(currencyRate.getRate());
        return calculatedValue.setScale(3, RoundingMode.HALF_UP).toPlainString();
    }

    public void onCurrencyValueChanged(String currencyCode, String newValue) {
        if (!baseCurrencyCode.equals(currencyCode)) {
            return;
        }
        try {
            baseValue = new BigDecimal(newValue);
        } catch (NumberFormatException e) {
            baseValue = BigDecimal.ZERO;
        }
    }
}
