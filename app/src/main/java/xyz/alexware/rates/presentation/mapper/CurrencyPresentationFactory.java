package xyz.alexware.rates.presentation.mapper;

import javax.inject.Inject;

import xyz.alexware.rates.R;
import xyz.alexware.rates.presentation.model.CurrencyView;

public class CurrencyPresentationFactory {

    @Inject
    CurrencyPresentationFactory() {
    }

    public CurrencyView getCurrencyView(String currencyCode) {
        switch (currencyCode) {
            case "AUD":
                return new CurrencyView(currencyCode, R.string.currency_australian_dollar, R.drawable.ic_australia);
            case "BGN":
                return new CurrencyView(currencyCode, R.string.currency_bulgarian_lev, R.drawable.ic_bulgaria);
            case "BRL":
                return new CurrencyView(currencyCode, R.string.currency_brazilian_real, R.drawable.ic_brazil);
            case "CAD":
                return new CurrencyView(currencyCode, R.string.currency_canadian_dollar, R.drawable.ic_canada);
            case "CHF":
                return new CurrencyView(currencyCode, R.string.currency_swiss_franc, R.drawable.ic_switzerland);
            case "CNY":
                return new CurrencyView(currencyCode, R.string.currency_chinese_yuan, R.drawable.ic_china);
            case "CZK":
                return new CurrencyView(currencyCode, R.string.currency_czech_koruna, R.drawable.ic_czech_republic);
            case "DKK":
                return new CurrencyView(currencyCode, R.string.currency_danish_krone, R.drawable.ic_denmark);
            case "EUR":
                return new CurrencyView(currencyCode, R.string.currency_euro, R.drawable.ic_european_union);
            case "GBP":
                return new CurrencyView(currencyCode, R.string.currency_pound_sterling, R.drawable.ic_united_kingdom);
            case "HKD":
                return new CurrencyView(currencyCode, R.string.currency_hong_kong_dollar, R.drawable.ic_hong_kong);
            case "HRK":
                return new CurrencyView(currencyCode, R.string.currency_croatian_kuna, R.drawable.ic_croatia);
            case "HUF":
                return new CurrencyView(currencyCode, R.string.currency_hungarian_forint, R.drawable.ic_hungary);
            case "IDR":
                return new CurrencyView(currencyCode, R.string.currency_indonesian_rupiah, R.drawable.ic_indonesia);
            case "ILS":
                return new CurrencyView(currencyCode, R.string.currency_israeli_new_shekel, R.drawable.ic_israel);
            case "INR":
                return new CurrencyView(currencyCode, R.string.currency_indian_rupee, R.drawable.ic_india);
            case "ISK":
                return new CurrencyView(currencyCode, R.string.currency_icelandic_krona, R.drawable.ic_iceland);
            case "JPY":
                return new CurrencyView(currencyCode, R.string.currency_japanese_yen, R.drawable.ic_japan);
            case "KRW":
                return new CurrencyView(currencyCode, R.string.currency_south_korean_won, R.drawable.ic_south_korea);
            case "MXN":
                return new CurrencyView(currencyCode, R.string.currency_mexican_peso, R.drawable.ic_mexico);
            case "MYR":
                return new CurrencyView(currencyCode, R.string.currency_malaysian_ringgit, R.drawable.ic_malaysia);
            case "NOK":
                return new CurrencyView(currencyCode, R.string.currency_norwegian_krone, R.drawable.ic_norway);
            case "NZD":
                return new CurrencyView(currencyCode, R.string.currency_new_zealand_dollar, R.drawable.ic_new_zealand);
            case "PHP":
                return new CurrencyView(currencyCode, R.string.currency_philippine_peso, R.drawable.ic_philippines);
            case "PLN":
                return new CurrencyView(currencyCode, R.string.currency_polish_zloty, R.drawable.ic_republic_of_poland);
            case "RON":
                return new CurrencyView(currencyCode, R.string.currency_romanian_leu, R.drawable.ic_romania);
            case "RUB":
                return new CurrencyView(currencyCode, R.string.currency_russian_ruble, R.drawable.ic_russia);
            case "SEK":
                return new CurrencyView(currencyCode, R.string.currency_swedish_krona, R.drawable.ic_sweden);
            case "SGD":
                return new CurrencyView(currencyCode, R.string.currency_singapore_dollar, R.drawable.ic_singapore);
            case "THB":
                return new CurrencyView(currencyCode, R.string.currency_thai_baht, R.drawable.ic_thailand);
            case "TRY":
                return new CurrencyView(currencyCode, R.string.currency_turkish_lira, R.drawable.ic_turkey);
            case "USD":
                return new CurrencyView(currencyCode, R.string.currency_united_states_dollar, R.drawable.ic_united_states_of_america);
            case "ZAR":
                return new CurrencyView(currencyCode, R.string.currency_south_african_rand, R.drawable.ic_south_africa);
        }
        return new CurrencyView(currencyCode, R.string.currency_unknown, R.drawable.ic_unknown);
    }
}
