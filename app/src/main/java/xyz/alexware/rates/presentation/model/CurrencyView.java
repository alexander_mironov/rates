package xyz.alexware.rates.presentation.model;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

public class CurrencyView {

    private final String code;
    private final int fullNameRes;
    private final int iconRes;

    public CurrencyView(String code, @StringRes int nameRes, @DrawableRes int iconRes) {
        this.code = code;
        this.fullNameRes = nameRes;
        this.iconRes = iconRes;
    }

    public String getCode() {
        return code;
    }

    @StringRes
    public int getNameRes() {
        return fullNameRes;
    }

    @DrawableRes
    public int getIconRes() {
        return iconRes;
    }
}
