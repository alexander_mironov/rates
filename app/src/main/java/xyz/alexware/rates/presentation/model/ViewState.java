package xyz.alexware.rates.presentation.model;

import java.util.List;

public class ViewState {
    private final State state;
    private final List<CurrencyRateView> data;
    private final Throwable error;
    private ViewState(State state, List<CurrencyRateView> data, Throwable error) {
        this.state = state;
        this.data = data;
        this.error = error;
    }

    public static ViewState success(List<CurrencyRateView> data) {
        return new ViewState(State.NORMAL, data, null);
    }

    public static ViewState error(Throwable error) {
        return new ViewState(State.ERROR, null, error);
    }

    public State getState() {
        return state;
    }

    public List<CurrencyRateView> getData() {
        return data;
    }

    public Throwable getError() {
        return error;
    }

    public enum State {
        NORMAL, ERROR
    }
}
