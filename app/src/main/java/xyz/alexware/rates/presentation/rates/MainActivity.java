package xyz.alexware.rates.presentation.rates;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import xyz.alexware.rates.CurrencyRateApplication;
import xyz.alexware.rates.R;
import xyz.alexware.rates.presentation.model.ViewState;

public class MainActivity extends AppCompatActivity implements CurrencyRateAdapter.Listener {

    @Inject
    CurrencyRatePresenter currencyRatePresenter;
    private CurrencyRateAdapter adapter;
    private RecyclerView recyclerView;
    private Disposable updateViewDisposable = Disposables.empty();
    private Snackbar errorSnackbar;
    private String lastErrorMessage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((CurrencyRateApplication) getApplication()).getApplicationComponent().inject(this);
        setupUi();
    }

    private void setupUi() {
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        DefaultItemAnimator defaultItemAnimator = new DefaultItemAnimator();
        defaultItemAnimator.setSupportsChangeAnimations(false); // prevents widgets from blinking
        recyclerView.setItemAnimator(defaultItemAnimator);

        adapter = new CurrencyRateAdapter();
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        currencyRatePresenter.startUpdatingRates();
        updateViewDisposable = currencyRatePresenter
                .getViewState()
                .subscribe(this::renderUi);
    }

    @Override
    protected void onStop() {
        super.onStop();
        currencyRatePresenter.stopUpdatingRates();
        updateViewDisposable.dispose();
    }

    private void renderUi(ViewState viewState) {
        switch (viewState.getState()) {
            case NORMAL:
                clearError();
                adapter.update(viewState.getData());
                break;
            case ERROR:
                Throwable error = viewState.getError();
                showSnackbarIfNewError(error);
                break;
        }

    }

    private void clearError() {
        lastErrorMessage = "";
        hideErrorSnackbar();
    }

    private void showSnackbarIfNewError(Throwable error) {
        if (!lastErrorMessage.equals(error.getLocalizedMessage())) {
            lastErrorMessage = error.getLocalizedMessage();
            showErrorSnackbar(error);
        }
    }

    private void hideErrorSnackbar() {
        if (errorSnackbar != null && errorSnackbar.isShownOrQueued()) {
            errorSnackbar.dismiss();
        }
    }

    private void showErrorSnackbar(Throwable error) {
        errorSnackbar = Snackbar.make(recyclerView, error.getLocalizedMessage(), Snackbar.LENGTH_INDEFINITE);
        errorSnackbar.show();
    }

    @Override
    public void onCurrencyClick(String currencyCode) {
        currencyRatePresenter.onCurrencySelected(currencyCode);
        adapter.makeItemTopmost(currencyCode);
        recyclerView.scrollToPosition(0);
    }

    @Override
    public void onCurrencyValueChanged(String currencyCode, String newValue) {
        currencyRatePresenter.onCurrencyValueChanged(currencyCode, newValue);
    }
}
