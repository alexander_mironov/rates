package xyz.alexware.rates.presentation.rates;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import xyz.alexware.rates.R;
import xyz.alexware.rates.presentation.model.CurrencyRateView;
import xyz.alexware.rates.utils.TextChangedWatcher;

public class CurrencyRateAdapter extends RecyclerView.Adapter<CurrencyRateAdapter.RateViewHolder> {

    private ArrayList<CurrencyRateView> items = new ArrayList<>();
    private Listener listener;

    @NonNull
    @Override
    public RateViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int index) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = layoutInflater.inflate(R.layout.item_currency_rate, viewGroup, false);
        RateViewHolder viewHolder = new RateViewHolder(itemView);
        setupListeners(viewHolder);
        return viewHolder;
    }

    private void setupListeners(RateViewHolder viewHolder) {
        viewHolder.panel.setOnClickListener(v -> {
            String currencyCode = (String) viewHolder.panel.getTag();
            String value = viewHolder.rate.getText().toString();
            notifyEvents(currencyCode, value);
            focusAndShowKeyboard(viewHolder.rate);
        });
        viewHolder.rate.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                String currencyCode = (String) viewHolder.panel.getTag();
                String value = viewHolder.rate.getText().toString();
                notifyEvents(currencyCode, value);
            }
        });
        viewHolder.rate.addTextChangedListener(new TextChangedWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                String currencyCode = (String) viewHolder.panel.getTag();
                String value = viewHolder.rate.getText().toString();
                notifyValueChange(currencyCode, value);
            }
        });
    }

    private void notifyEvents(String currencyCode, String newValue) {
        notifyClick(currencyCode);
        notifyValueChange(currencyCode, newValue);
    }

    private static void focusAndShowKeyboard(EditText editText) {
        Context context = editText.getContext();
        editText.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    private void notifyValueChange(String currencyCode, String newValue) {
        if (listener != null) {
            listener.onCurrencyValueChanged(currencyCode, newValue);
        }
    }

    private void notifyClick(String currencyCode) {
        if (listener != null) {
            listener.onCurrencyClick(currencyCode);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RateViewHolder viewHolder, int index) {
        CurrencyRateView currencyRateView = items.get(index);
        viewHolder.panel.setTag(currencyRateView.getCode());
        viewHolder.currencyCode.setText(currencyRateView.getCode());
        viewHolder.currencyName.setText(currencyRateView.getNameRes());
        Drawable icon = AppCompatResources.getDrawable(viewHolder.currencyIcon.getContext(), currencyRateView.getIconRes());
        viewHolder.currencyIcon.setImageDrawable(icon);
        viewHolder.rate.setText(currencyRateView.getCalculatedValue());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void makeItemTopmost(@NonNull String currencyCode) {
        int currentPosition = getPosition(currencyCode, items);
        if (alreadyTopmost(currentPosition)) {
            return;
        }
        CurrencyRateView currencyRateView = items.remove(currentPosition);
        items.add(0, currencyRateView);
        notifyItemMoved(currentPosition, 0);
    }

    private static int getPosition(@NonNull String currencyCode, List<CurrencyRateView> items) {
        for (int i = 0; i < items.size(); i++) {
            if (currencyCode.equals(items.get(i).getCode())) {
                return i;
            }
        }
        return -1;
    }

    private boolean alreadyTopmost(int position) {
        return position == 0;
    }

    public void update(List<CurrencyRateView> newRates) {
        if (newRates.size() > 0) {
            items = new ArrayList<>(newRates);
            notifyItemRangeChanged(1, items.size() - 1); // do not update topmost to prevent keyboard flickering
        }
    }

    public interface Listener {
        void onCurrencyClick(String currencyCode);

        void onCurrencyValueChanged(String currencyCode, String newValue);
    }

    class RateViewHolder extends RecyclerView.ViewHolder {

        private final TextView currencyCode;
        private final TextView currencyName;
        private final AppCompatImageView currencyIcon;
        private final EditText rate;
        private final View panel;

        RateViewHolder(@NonNull View itemView) {
            super(itemView);
            currencyCode = itemView.findViewById(R.id.currencyCode);
            currencyName = itemView.findViewById(R.id.currencyName);
            currencyIcon = itemView.findViewById(R.id.currencyIcon);
            rate = itemView.findViewById(R.id.rate);
            panel = itemView.findViewById(R.id.panel);
        }
    }
}
