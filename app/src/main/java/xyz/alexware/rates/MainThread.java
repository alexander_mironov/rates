package xyz.alexware.rates;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import xyz.alexware.rates.domain.executor.PostExecutionThread;

public class MainThread implements PostExecutionThread {

    @Inject
    MainThread() {
    }

    @Override
    public Scheduler getScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
