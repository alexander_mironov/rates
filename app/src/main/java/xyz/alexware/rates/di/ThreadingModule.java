package xyz.alexware.rates.di;

import dagger.Binds;
import dagger.Module;
import xyz.alexware.rates.MainThread;
import xyz.alexware.rates.domain.executor.PostExecutionThread;

@Module
public abstract class ThreadingModule {
    @Binds
    public abstract PostExecutionThread bindPostExecutionThread(MainThread mainThread);
}
