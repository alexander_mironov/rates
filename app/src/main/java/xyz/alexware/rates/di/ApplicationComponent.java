package xyz.alexware.rates.di;

import javax.inject.Singleton;

import dagger.Component;
import xyz.alexware.rates.presentation.rates.MainActivity;

@Singleton
@Component(modules = {NetworkModule.class, ThreadingModule.class})
public interface ApplicationComponent {
    void inject(MainActivity mainActivity);
}
