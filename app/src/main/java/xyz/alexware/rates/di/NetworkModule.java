package xyz.alexware.rates.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import xyz.alexware.rates.data.NetworkCurrencyRatesRepository;
import xyz.alexware.rates.data.mapper.RatesMapper;
import xyz.alexware.rates.data.service.CurrencyRatesService;
import xyz.alexware.rates.domain.repository.CurrencyRatesRepository;

@Module
class NetworkModule {

    @Singleton
    @Provides
    Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl("https://revolut.duckdns.org")
                .build();
    }

    @Singleton
    @Provides
    CurrencyRatesService provideCurrencyRatesService(Retrofit retrofit) {
        return retrofit.create(CurrencyRatesService.class);
    }

    @Singleton
    @Provides
    CurrencyRatesRepository provideCurrencyRatesRepository(CurrencyRatesService currencyRatesService,
                                                           RatesMapper ratesMapper) {
        return new NetworkCurrencyRatesRepository(currencyRatesService, ratesMapper);
    }

}
