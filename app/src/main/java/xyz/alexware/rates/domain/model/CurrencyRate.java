package xyz.alexware.rates.domain.model;

import java.math.BigDecimal;

public class CurrencyRate {
    private final String currencyCode;
    private final BigDecimal rate;

    public CurrencyRate(String currencyCode, BigDecimal rate) {
        this.currencyCode = currencyCode;
        this.rate = rate;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public BigDecimal getRate() {
        return rate;
    }
}
