package xyz.alexware.rates.domain.model;

import android.support.annotation.NonNull;

import java.util.ArrayList;

public class CurrencyRateList extends ArrayList<CurrencyRate> {

    public CurrencyRate get(@NonNull String currencyCode) {
        for (CurrencyRate currencyRate : this) {
            if (currencyCode.equals(currencyRate.getCurrencyCode())) {
                return currencyRate;
            }
        }
        return null;
    }
}
