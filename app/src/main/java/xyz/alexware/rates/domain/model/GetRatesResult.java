package xyz.alexware.rates.domain.model;

public class GetRatesResult {
    private final State state;
    private final CurrencyRateList data;
    private final Throwable error;
    private GetRatesResult(State state, CurrencyRateList data, Throwable error) {
        this.state = state;
        this.data = data;
        this.error = error;
    }

    public static GetRatesResult success(CurrencyRateList data) {
        return new GetRatesResult(State.NORMAL, data, null);
    }

    public static GetRatesResult error(Throwable error) {
        return new GetRatesResult(State.ERROR, null, error);
    }

    public State getState() {
        return state;
    }

    public CurrencyRateList getData() {
        return data;
    }

    public Throwable getError() {
        return error;
    }

    public enum State {
        NORMAL, ERROR
    }
}
