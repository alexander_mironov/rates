package xyz.alexware.rates.domain.interactor.rates;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import xyz.alexware.rates.domain.executor.PostExecutionThread;
import xyz.alexware.rates.domain.model.GetRatesResult;
import xyz.alexware.rates.domain.repository.CurrencyRatesRepository;

public class GetRates {

    private final PostExecutionThread postExecutionThread;
    private final CurrencyRatesRepository currencyRatesRepository;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    GetRates(CurrencyRatesRepository currencyRatesRepository, PostExecutionThread postExecutionThread) {
        this.postExecutionThread = postExecutionThread;
        this.currencyRatesRepository = currencyRatesRepository;
    }

    public void execute(Consumer<GetRatesResult> onNextConsumer, String baseCurrency) {
        Observable<GetRatesResult> observable = buildUseCaseObservable(baseCurrency)
                .subscribeOn(Schedulers.io())
                .observeOn(postExecutionThread.getScheduler());
        addDisposable(observable.subscribe(onNextConsumer));
    }

    private Observable<GetRatesResult> buildUseCaseObservable(String baseCurrency) {
        return getCurrencyRateListObservable(baseCurrency);
    }

    private Observable<GetRatesResult> getCurrencyRateListObservable(String baseCurrency) {
        return Observable.interval(1, TimeUnit.SECONDS)
                .concatMapSingle(i -> currencyRatesRepository.getLatestRates(baseCurrency))
                .map(GetRatesResult::success)
                .onErrorResumeNext(throwable -> {
                    return getCurrencyRateListObservable(baseCurrency)
                            .startWith(GetRatesResult.error(throwable));
                });
    }

    private void addDisposable(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    public void clear() {
        compositeDisposable.clear();
    }
}
