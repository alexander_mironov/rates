package xyz.alexware.rates.domain.repository;

import io.reactivex.Single;
import xyz.alexware.rates.domain.model.CurrencyRateList;

public interface CurrencyRatesRepository {
    Single<CurrencyRateList> getLatestRates(String baseCurrency);
}
